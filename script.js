var xhr = new XMLHttpRequest();
xhr.open('GET', 'http://localhost/test/api.html');
xhr.responseType = 'json';
xhr.send();
xhr.onload = function() {
    var users = xhr.response;
    userCard(users);
}


function userCard(jsonObj) {
    var tableBody = document.querySelector('tbody');
    for (var i = 0; i < jsonObj.length; i++) {
        var tr = document.createElement('tr');
        tableBody.appendChild(tr);
        var th = document.createElement('th');
        th.innerHTML = i + 1;
        tr.appendChild(th);
        var td = document.createElement('td');
        td.innerHTML = jsonObj[i].fullname;
        tr.appendChild(td);
        var td = document.createElement('td');
        td.innerHTML = jsonObj[i]['role_id'];
        tr.appendChild(td);
        var td = document.createElement('td');
        var buttonsContainer = document.createElement('div');
        buttonsContainer.className = 'container-buttons';
        var btnEdit = document.createElement('button');
        btnEdit.innerHTML = 'Редактировать';
        btnEdit.className = 'btn btn-outline-info';
        btnEdit.id = i;
        var editRef = document.createElement('a');
        editRef.href = 'http://localhost/test/update_user.html?id=' + btnEdit.id;
        editRef.appendChild(btnEdit);
        var btnDel = document.createElement('button');
        btnDel.innerHTML = 'Удалить';
        btnDel.className = 'btn btn-outline-danger';
        btnDel.id = jsonObj[i].id;
        buttonsContainer.appendChild(editRef);
        buttonsContainer.appendChild(btnDel);
        td.appendChild(buttonsContainer);
        tr.appendChild(td);
        btnDel.addEventListener('click', deleteUser);
    }
}

function deleteUser() {
    var conf = confirm('Вы точно хотите удалить данную запись?');
    if (conf === true) {
        $.ajax({
            type: 'POST',
            url: 'http://localhost/test/delete_user_server.html',
            data: {
              user_id: this.id
            },
            success: function() {
                location.reload();
            }
        });
    }
}